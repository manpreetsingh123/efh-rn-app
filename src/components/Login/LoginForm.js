import React from "react";
import { useNavigation } from "@react-navigation/native";
import { StyleSheet, View, Button, Text } from "react-native";

import { useDispatch } from "react-redux";

import { signin } from "../../store/slices/User.slice";

const LoginForm = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const loginHandler = () => {
    dispatch(signin());
  };

  const forgotPasswordHandler = () => {
    navigation.navigate("ForgotPassword");
  };

  const ActionButton = ({ onPress, title }) => (
    <View style={styles.action}>
      <Button {...{ onPress, title }} />
    </View>
  );

  return (
    <View>
      <Text>Login in View</Text>
      <View style={styles.actionContainer}>
        <ActionButton title="Forgot Password" onPress={forgotPasswordHandler} />
        <ActionButton title="Log In" onPress={loginHandler} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  actionContainer: {
    display: "flex",
    flexDirection: "column",
  },
  action: {
    margin: 5,
  },
});

export default LoginForm;
