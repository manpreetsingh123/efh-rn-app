import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";

import reducer from "./slices/";

import timerMiddleware from "./middlewares/Timer.middleware";

// Get default middlewares provided by redux toolkit i.e. thunk, immutableCheck and serializableCheck
const defaultMiddlewares = getDefaultMiddleware();

// Custom middlewares
const appMiddlewares = [timerMiddleware];

// Combine middlewares
const middleware = [...defaultMiddlewares, ...appMiddlewares];

// Create store
const appStore = configureStore({
  reducer,
  middleware,
});

export default appStore;
