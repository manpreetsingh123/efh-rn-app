import { createSlice } from "@reduxjs/toolkit";

// Initial User State
const initialState = {
  userId: null,
  userData: null,
  loggedIn: false,
};

// User redux state implementation
const UserSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    signin: (state) => {
      state.loggedIn = true;
    },
    signout: (state, action) => {
      state.loggedIn = false;
    },
  },
});

// Exporting all users action creaters
export const { signin, signout } = UserSlice.actions;

// Exprting user redux reducer
export default UserSlice.reducer;
