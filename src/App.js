import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import { useSelector } from 'react-redux';

import LoginStack from '~/navigation/LoginStack';
import DashboardStack from '~/navigation/DashboardStack';

export default function App() {
    const isLoggedIn = useSelector((state) => state.user.loggedIn);

    console.log(Platform.OS);

    if (Platform.OS == "web") {
        if (navigator.userAgent.toLowerCase().indexOf('electron/') > -1) {
            console.log(window.process);
            const { ipcRenderer } = window.require('electron');

            // Synchronous message emmiter and handler
            // ipcRenderer.sendSync('synchronous-message', 'sync ping')

            //Async message sender
            ipcRenderer.send('asynchronous-message', 'async ping')
        }
    }

    return (
        <View style={styles.container}>
            {isLoggedIn
                ? <DashboardStack/>
                : <LoginStack />}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
});