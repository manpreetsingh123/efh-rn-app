import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import UserDashboardView from "~/screens/UserDashboard/UserDashboardView";
import { useDispatch } from "react-redux";
import { signout } from "~/store/slices/User.slice";
import { Button, View, StyleSheet } from "react-native";

const Stack = createStackNavigator();

const DashboardStack = () => {
  const dispatch = useDispatch();

  const logoutHandler = () => {
    dispatch(signout());
  };

  const headerRight = (props) => {
    return (
      <View style={styles.headerRightContainer}>
        <Button title="Logout" onPress={logoutHandler} />
      </View>
    );
  };

  return (
    <Stack.Navigator
      initialRouteName="UserDashboard"
      screenOptions={{
        headerRight,
      }}
    >
      <Stack.Screen name="UserDashboard" component={UserDashboardView} />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  headerRightContainer: {
    paddingHorizontal: 20,
  },
});

export default DashboardStack;
