import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginView from '~/screens/Login/LoginView'
import ForgotPasswordView from '~/screens/Login/ForgotPasswordView'

const Stack = createStackNavigator();

const LoginStack = () => {
    return (
        <Stack.Navigator initialRouteName="Login">
            <Stack.Screen name="Login" component={LoginView} />
            <Stack.Screen name="ForgotPassword" component={ForgotPasswordView} />
        </Stack.Navigator>
    );
}

export default LoginStack;