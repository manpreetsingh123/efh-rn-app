import "react-native-gesture-handler";
import React, { Component } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { Provider } from "react-redux";

import App from "./App";

import appStore from "./store/";

export default class AppContainer extends Component {
  render() {
    return (
      <Provider store={appStore}>
        <NavigationContainer>
          <App />
        </NavigationContainer>
      </Provider>
    );
  }
}
